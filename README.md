# Binary Classification incl. Locking Mechanism

1. Takes an image and checks (with a pretrained neular network) whether the face is alive or not.  
2. If the networks confidence is higher than 96%, the person is considered alive.
3. If the face was classified as alive, the Face Recognition starts.
4. If it is someone the system knows, the person is allowed to enter.

A 3D convolutional network is used for the classification task. (It has to be 3D, because features, like eye movement, winkings etc. aren't included.)

## Prerequisites

This project requires the following dependencies to be installed beforehand:

* Python and pip  
* vs2017 for Windows
* CMake
* xQuartz/X11

## Dependencies

* OpenCV-Python
* NumPy
* Face_Recognition
* Keras

To install all the needed dependencies run:

```
pip install -r requirements.txt
```

## Usage

* Once the dependencies are installed (via pip) start the app with:

```
python main.py
```

## Sources  

* [Adrian Rosebrock - pyimagesearch](https://www.pyimagesearch.com/2019/03/11/liveness-detection-with-opencv/)